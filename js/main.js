$(function(){
	toggleMobileMenu();
	initSlickSkider()
});

function toggleMobileMenu() {
	$('.toggle-btn').on('click',function(){
		$('.menu').slideToggle(100);
	})
}

function initSlickSkider() {
	$('.our-team-slider').slick({
		dots: true,
		arrows: false
  	});
}